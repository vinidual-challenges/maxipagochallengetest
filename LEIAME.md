# MaxiPagoChallengeTest

## Introdução

Esse foi um teste realizado como uma primeira fase de avaliação para uma vaga de desenvolvedor Java, sem especificação do nível de cargo.

## Objetivo

Fornecer webservice que retorna lista com combinação de pares de cidades e distância entre elas. 

## Requisitos

- Webservice RESTful
- Java 8 ou superior
- Tomcat 8 ou superior
- Parâmetro obrigatório para definir a unidade de medida (km / mi)
- Parâmetro obrigatório para definir o formato de resposta (xml / json)
- Banco de dados do MySQL com apenas uma tabela (CITY [id, name, latitude, longitude]) e com um mínimo de 3 registros
- Use apenas acesso via JDBC - Não use framework de persistência, pool de conexões ou similares.
- Você pode pesquisar na web pelo método de cálculo de distância, mas precisa justificar sua escolha.
- Justifique o método de combinação implementado com foco em desempenho e simplicidade.
- Entregue dois arquivos de texto com o projeto README.TXT e LEIAME.TXT contendo as justificativas, scripts e observações em inglês e português, respectivamente.
- O projeto deve ser entregue com o código fonte no arquivo war.

## Solução

A solução foi construída utilizando Java 11 com tomcat embedded e spring boot.

Foi construída uma api restfull para um CRUD para cidades seguindo boas práticas de níveis de maturidade através do seguinte artigo:

- https://blog.restcase.com/4-maturity-levels-of-rest-api-design/

Para o cálculo da distância entre pares de cidades foi utilizado a fórmula de Haversine através do artigo:

- https://pt.wikipedia.org/wiki/F%C3%B3rmula_de_Haversine

O método apresenta resultados interessantes com boas aproximações para pontos flutuantes, além de ser significativamente simples de ser implementada em qualquer linguagem de programação.

Para auxiliar nos testes da api é possível consultar as requisições que a aplicação utiliza no seguinte artigo:

- https://documenter.getpostman.com/view/1454530/S1LvVTxA#af71304a-d93a-4ff2-bcf8-97ee015efb6b

Os testes foram realizados em ambiente local.

Note que os parâmetros para solicitar a resposta em formato JSON ou XML é definito pelo header de ACCEPT que é definido pelos seguintes valores:

- application/json
- application/xml

## Configuração

É necessário configurar um banco mysql local para executar os testes de integração e subir a aplicação:

- spring.datasource.driver = com.mysql.cj.jdbc.Driver
- spring.datasource.url = jdbc:mysql://localhost:3306/maxipago
- spring.datasource.username = maxipago
- spring.datasource.password = maxipago
