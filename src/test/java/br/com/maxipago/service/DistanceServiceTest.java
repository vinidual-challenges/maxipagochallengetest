package br.com.maxipago.service;

import br.com.maxipago.model.City;
import br.com.maxipago.model.Distance;
import br.com.maxipago.model.MetricUnit;
import br.com.maxipago.repository.CityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
public class DistanceServiceTest {

    @InjectMocks
    DistanceService distanceService;

    @Mock
    CityRepository cityRepository;

    private String saoPaulo = "Sao Paulo";
    private String rioDeJaneiro = "Rio de Janeiro";
    private String nagasaki = "Nagasaki";

    private Double saoPauloRioKm = 357.00;
    private Double saoPauloRioMi = 222.00;

    private Double saoPauloNagasakiKm = 18934.00;
    private Double saoPauloNagasakiMi = 11765.00;

    private Double rioNagasakiKm = 18724.00;
    private Double rioNagasakiMi = 11635.00;

    @Test
    public void shouldCalculateDistanceMi() {

        Mockito.when(cityRepository.getAll()).thenReturn(createCities());

        List<Distance> distances = distanceService.getAllDistances(MetricUnit.MI)
                .stream().collect(Collectors.toList());

        Optional<Distance> saoPauloRio = distances.stream().filter(c ->
                c.getCityOrigin().equals(saoPaulo) && c.getCityDestiny().equals(rioDeJaneiro) ||
                    c.getCityOrigin().equals(rioDeJaneiro) && c.getCityDestiny().equals(saoPaulo))
                .findFirst();

        Optional<Distance> saoPauloNagasaki = distances.stream().filter(c ->
                c.getCityOrigin().equals(saoPaulo) && c.getCityDestiny().equals(nagasaki) ||
                        c.getCityOrigin().equals(nagasaki) && c.getCityDestiny().equals(saoPaulo))
                .findFirst();

        Optional<Distance> rioNagasaki = distances.stream().filter(c ->
                c.getCityOrigin().equals(rioDeJaneiro) && c.getCityDestiny().equals(nagasaki) ||
                        c.getCityOrigin().equals(nagasaki) && c.getCityDestiny().equals(rioDeJaneiro))
                .findFirst();

        Assert.assertTrue(saoPauloRio.get().getDistance() >= saoPauloRioMi - 1
                && saoPauloRio.get().getDistance() <= saoPauloRioMi + 1);

        Assert.assertTrue(saoPauloNagasaki.get().getDistance() >= saoPauloNagasakiMi - 2
                && saoPauloNagasaki.get().getDistance() <= saoPauloNagasakiMi + 2);

        Assert.assertTrue(rioNagasaki.get().getDistance() >= rioNagasakiMi - 2
                && rioNagasaki.get().getDistance() <= rioNagasakiMi + 2);
    }

    @Test
    public void shouldCalculateDistanceKm() {

        Mockito.when(cityRepository.getAll()).thenReturn(createCities());

        List<Distance> distances = distanceService.getAllDistances(MetricUnit.KM)
                .stream().collect(Collectors.toList());

        Optional<Distance> saoPauloRio = distances.stream().filter(c ->
                c.getCityOrigin().equals(saoPaulo) && c.getCityDestiny().equals(rioDeJaneiro) ||
                        c.getCityOrigin().equals(rioDeJaneiro) && c.getCityDestiny().equals(saoPaulo))
                .findFirst();

        Optional<Distance> saoPauloNagasaki = distances.stream().filter(c ->
                c.getCityOrigin().equals(saoPaulo) && c.getCityDestiny().equals(nagasaki) ||
                        c.getCityOrigin().equals(nagasaki) && c.getCityDestiny().equals(saoPaulo))
                .findFirst();

        Optional<Distance> rioNagasaki = distances.stream().filter(c ->
                c.getCityOrigin().equals(rioDeJaneiro) && c.getCityDestiny().equals(nagasaki) ||
                        c.getCityOrigin().equals(nagasaki) && c.getCityDestiny().equals(rioDeJaneiro))
                .findFirst();

        Assert.assertTrue(saoPauloRio.get().getDistance() >= saoPauloRioKm - 1
                && saoPauloRio.get().getDistance() <= saoPauloRioKm + 1);

        Assert.assertTrue(saoPauloNagasaki.get().getDistance() >= saoPauloNagasakiKm - 2
                && saoPauloNagasaki.get().getDistance() <= saoPauloNagasakiKm + 2);

        Assert.assertTrue(rioNagasaki.get().getDistance() >= rioNagasakiKm - 2
                && rioNagasaki.get().getDistance() <= rioNagasakiKm + 2);
    }

    private Set<City> createCities() {

        Set<City> cities = new HashSet<>();

        cities.add(new City(null, "Sao Paulo", "-23.5505", "-46.6333"));
        cities.add(new City(null, "Rio de Janeiro", "-22.9035", "-43.2096"));
        cities.add(new City(null, "Nagasaki", "32.764233", "129.872696"));

        return cities;
    }

}
