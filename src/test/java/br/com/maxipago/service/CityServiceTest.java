package br.com.maxipago.service;

import br.com.maxipago.model.City;
import br.com.maxipago.repository.CityRepository;
import br.com.maxipago.service.CityService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {

    @InjectMocks
    CityService cityService;

    @Mock
    CityRepository cityRepository;

    @Test
    public void shouldCreateCity(){

        City city = new City(1L, "name", "lat", "long");

        Mockito.when(cityRepository.create(city)).thenReturn(city);

        City cityUpdate = cityService.create(city);

        Assert.assertEquals(cityUpdate, city);
    }

    @Test
    public void shouldGetCity(){

        Long id = 1L;

        City city = new City(1L, "name", "lat", "long");

        Mockito.when(cityRepository.get(id)).thenReturn(city);

        City cityReturned = cityService.get(id);

        Assert.assertEquals(city, cityReturned);
    }

    @Test
    public void shouldGetAllCities(){

        Set<City> city = cityService.getAll();

        Assert.assertEquals(new HashSet<>(), city);
    }

    @Test
    public void shouldUpdateCity(){

        City city = new City(1L, "name", "lat", "long");

        city = cityService.update(city);

        Assert.assertEquals(null, city);
    }

    @Test
    public void shouldDeleteCity(){
        cityService.delete(1L);
    }

}
