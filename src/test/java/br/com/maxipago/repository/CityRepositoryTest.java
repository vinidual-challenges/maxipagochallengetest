package br.com.maxipago.repository;

import br.com.maxipago.configuration.DatabaseEmbeddedConfiguration;
import br.com.maxipago.model.City;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CityRepositoryTest {

    @Autowired
    CityRepository cityRepository;

    @Autowired
    DatabaseEmbeddedConfiguration databaseEmbeddedConfiguration;

    Long id = null;

    String name = "name";
    String latitude = "latitude";
    String longitude = "longitude";

    String nameUpdated = "nameUpdated";
    String latitudeUpdated = "latitudeUpdated";
    String longitudeUpdated = "longitudeUpdated";

    @Before
    public void setup(){
        cityRepository.dataSource = databaseEmbeddedConfiguration.dataSource();
    }

    @Test
    public void t01ShouldCreateCity(){

        City city = cityRepository.create(new City(id, name, latitude, longitude));

        Assert.assertNotNull(city);
        Assert.assertNotNull(city.getId());

        Assert.assertNotEquals(id, city.getId());

        Assert.assertEquals(name, city.getName());
        Assert.assertEquals(latitude, city.getLatitude());
        Assert.assertEquals(longitude, city.getLongitude());
    }

    @Test
    public void t02ShouldGetCreatedCity(){

        Long id = 4L;

        City city = cityRepository.get(id);

        Assert.assertNotNull(city);

        Assert.assertTrue(city.getId().equals(id));
        Assert.assertTrue(city.getName().equals(name));
        Assert.assertTrue(city.getLatitude().equals(latitude));
        Assert.assertTrue(city.getLongitude().equals(longitude));
    }

    @Test
    public void t03ShouldGetAllCreatedCities(){

        Set<City> cities = cityRepository.getAll();

        Assert.assertNotNull(cities);

        Assert.assertTrue(cities.size() == 4);
    }

    @Test
    public void t04ShouldUpdateCity(){

        City city = cityRepository.get(4L);

        City update = new City();

        update.setId(city.getId());
        update.setName(nameUpdated);
        update.setLatitude(latitudeUpdated);
        update.setLongitude(longitudeUpdated);

        City updated = cityRepository.update(update);

        Assert.assertNotNull(updated);
        Assert.assertNotNull(updated.getId());

        Assert.assertEquals(city.getId(), updated.getId());

        Assert.assertEquals(nameUpdated, updated.getName());
        Assert.assertEquals(latitudeUpdated, updated.getLatitude());
        Assert.assertEquals(longitudeUpdated, updated.getLongitude());
    }

    @Test
    public void t05ShouldGetAllCreatedCitiesAgain(){

        Set<City> cities = cityRepository.getAll();

        Assert.assertNotNull(cities);

        Assert.assertTrue(cities.size() == 4);
    }

    @Test
    public void t06ShouldDeleteCity(){

        Long id = 4L;

        cityRepository.delete(id);

        City city = cityRepository.get(4L);

        Assert.assertNull(city);
    }

    @Test
    public void t07ShouldGetAllCreatedCitiesOnceAgain(){

        Set<City> cities = cityRepository.getAll();

        Assert.assertNotNull(cities);

        Assert.assertTrue(cities.size() == 3);
    }

    @Test
    public void t08ShouldNotGetDeletedCity(){

        City city = cityRepository.get(4L);

        Assert.assertNull(city);
    }

    @Test
    public void t09ShouldNotUpdateDeletedCity(){

        City update = new City();

        update.setId(4L);
        update.setName(nameUpdated);
        update.setLatitude(latitudeUpdated);
        update.setLongitude(longitudeUpdated);

        City updated = cityRepository.update(update);

        Assert.assertNull(updated);
    }

    @Test
    public void t10ShouldNotCreateCityWithId(){
        City city = new City();

        city.setId(3L);
        city.setName(nameUpdated);
        city.setLatitude(latitudeUpdated);
        city.setLongitude(longitudeUpdated);

        City create = cityRepository.create(city);

        Assert.assertNull(create);
    }

}
