package br.com.maxipago.controller;

import br.com.maxipago.service.DistanceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;

@RunWith(MockitoJUnitRunner.class)
public class DistanceControllerTest {

    @InjectMocks
    DistanceController distanceController;

    @Mock
    DistanceService distanceService;

    private MockMvc mockMvc;

    private String getAll = "/getAllDistances";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(distanceController)
                .build();
    }

    @Test
    public void shouldGetAllDistancesKM() throws Exception {

        Mockito.when(distanceService.getAllDistances(Mockito.any())).thenReturn(new HashSet<>());

        mockMvc.perform(MockMvcRequestBuilders.get(getAll + "?metricUnit=KM")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldGetAllDistancesMI() throws Exception {

        Mockito.when(distanceService.getAllDistances(Mockito.any())).thenReturn(new HashSet<>());

        mockMvc.perform(MockMvcRequestBuilders.get(getAll + "?metricUnit=MI")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldNotGetAllDistancesNull() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get(getAll)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void shouldNotGetAllDistancesInvalid() throws Exception {

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(getAll + "?metricUnit=YD")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
