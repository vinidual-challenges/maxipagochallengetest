package br.com.maxipago.controller;

import br.com.maxipago.model.City;
import br.com.maxipago.service.CityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class CityControllerTest {

    @InjectMocks
    CityController cityController;

    @Mock
    CityService cityService;

    private MockMvc mockMvc;

    private String post = "/addNewCity";
    private String get = "/getCity/";
    private String getAll = "/getAllCities";
    private String put = "/updateCity";
    private String delete = "/deleteCity/";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(cityController)
                .build();
    }

    @Test
    public void shouldCreateCityJSON() throws Exception {

        City city = new City(1L, "name", "lat", "long");

        Mockito.when(cityService.create(Mockito.any())).thenReturn(city);

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(post)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(cityJSON))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void shouldCreateCityXML() throws Exception {

        City city = new City(2L, "name", "lat", "long");

        Mockito.when(cityService.create(Mockito.any())).thenReturn(city);

        String cityXML = new XmlMapper().writeValueAsString(city);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post(post)
                .contentType(MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_XML)
                .content(cityXML))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void shouldUpdateCityJSON() throws Exception {

        City city = new City(1L, "name", "lat", "long");

        Mockito.when(cityService.update(Mockito.any())).thenReturn(city);

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        mockMvc.perform(MockMvcRequestBuilders.put(put)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(cityJSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldUpdateCityXML() throws Exception {

        City city = new City(2L, "name", "lat", "long");

        Mockito.when(cityService.update(Mockito.any())).thenReturn(city);

        String cityXML = new XmlMapper().writeValueAsString(city);

        mockMvc.perform(MockMvcRequestBuilders.put(put)
                .contentType(MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_XML)
                .content(cityXML))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldGetCityJSON() throws Exception {

        City city = new City(1L, "name", "lat", "long");

        Mockito.when(cityService.get(city.getId())).thenReturn(city);

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(get + city.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldGetCityXML() throws Exception {

        City city = new City(2L, "name", "lat", "long");

        Mockito.when(cityService.get(city.getId())).thenReturn(city);

        String cityXML = new XmlMapper().writeValueAsString(city);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(get + city.getId())
                .accept(MediaType.APPLICATION_XML))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldGetAllCitiesJSON() throws Exception {

        Set<City> city = new HashSet<>();

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(getAll)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        String resultJSON = resultActions.andReturn().getResponse().getContentAsString();

        Assert.assertEquals(cityJSON, resultJSON);
    }

    @Test
    public void shouldGetAllCitiesXML() throws Exception {

        Set<City> city = new HashSet<>();

        String cityXML = new XmlMapper().writeValueAsString(city).replaceAll("HashSet", "Set");

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(getAll)
                .accept(MediaType.APPLICATION_XML))
                .andExpect(MockMvcResultMatchers.status().isOk());

        String resultXML = resultActions.andReturn().getResponse().getContentAsString();

        Assert.assertEquals(cityXML, resultXML);
    }

    @Test
    public void shouldDeleteCity() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.delete(delete + 1L))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

    }

}

