package br.com.maxipago.integration;

import br.com.maxipago.configuration.DatabaseEmbeddedConfiguration;
import br.com.maxipago.model.City;
import br.com.maxipago.repository.CityRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.json.JSONArray;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CityIntegrationTest {

    @LocalServerPort
    Integer port;

    RestTemplate restTemplate = new RestTemplate();

    String post = "/addNewCity";
    String get = "/getCity/";
    String getAll = "/getAllCities";
    String put = "/updateCity";
    String delete = "/deleteCity/";

    @Test
    public void t01ShouldCreateCityJSON() throws Exception {

        City city = new City(null, "name", "latitude", "longitude");

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<String> entity = new HttpEntity<String>(cityJSON, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrl(post), HttpMethod.POST, entity, String.class);

        City created = new ObjectMapper().readValue(response.getBody(), City.class);

        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assert.assertEquals(4L, created.getId().longValue());
        Assert.assertEquals("name", created.getName());
        Assert.assertEquals("latitude", created.getLatitude());
        Assert.assertEquals("longitude", created.getLongitude());
    }

    @Test
    public void t02ShouldCreateCityXML() throws Exception {

        City city = new City(null, "name", "latitude", "longitude");

        String cityXML = new XmlMapper().writeValueAsString(city);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML_VALUE);

        HttpEntity<String> entity = new HttpEntity<String>(cityXML, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrl(post), HttpMethod.POST, entity, String.class);

        City created = new XmlMapper().readValue(response.getBody(), City.class);

        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assert.assertEquals(5L, created.getId().longValue());
        Assert.assertEquals("name", created.getName());
        Assert.assertEquals("latitude", created.getLatitude());
        Assert.assertEquals("longitude", created.getLongitude());
    }

    @Test
    public void t03ShouldGetCityJSON() throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrl(get + 4L), HttpMethod.GET, entity, String.class);

        City found = new ObjectMapper().readValue(response.getBody(), City.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(4L, found.getId().longValue());
        Assert.assertEquals("name", found.getName());
        Assert.assertEquals("latitude", found.getLatitude());
        Assert.assertEquals("longitude", found.getLongitude());
    }

    @Test
    public void t04ShouldGetCityXML() throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML_VALUE);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrl(get + 5L), HttpMethod.GET, entity, String.class);

        City found = new XmlMapper().readValue(response.getBody(), City.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(5L, found.getId().longValue());
        Assert.assertEquals("name", found.getName());
        Assert.assertEquals("latitude", found.getLatitude());
        Assert.assertEquals("longitude", found.getLongitude());
    }

    @Test
    public void t05ShouldNotCreateCity() throws Exception {

        City city = new City(1L, "name", "latitude", "longitude");

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<String> entity = new HttpEntity<String>(cityJSON, headers);

        try {
            restTemplate.exchange(createUrl(post), HttpMethod.POST, entity, String.class);
        } catch (HttpClientErrorException ex) {
            Assert.assertEquals(HttpStatus.CONFLICT, ex.getStatusCode());
        }
    }

    @Test
    public void t06ShouldNotGetCity() throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        try {
            restTemplate.exchange(createUrl(get + 6L), HttpMethod.GET, entity, String.class);
        } catch (HttpClientErrorException ex) {
            Assert.assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }

    @Test
    public void t07ShouldUpdateCity() throws Exception {

        City city = new City(1L, "nameUpdated", "latitudeUpdated", "longitudeUpdated");

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<String> entity = new HttpEntity<>(cityJSON, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrl(put), HttpMethod.PUT, entity, String.class);

        City updated = new ObjectMapper().readValue(response.getBody(), City.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(1L, updated.getId().longValue());
        Assert.assertEquals("nameUpdated", updated.getName());
        Assert.assertEquals("latitudeUpdated", updated.getLatitude());
        Assert.assertEquals("longitudeUpdated", updated.getLongitude());
    }

    @Test
    public void t08ShouldNotUpdateCityConflict() throws Exception {

        City city = new City(null, "nameUpdated", "latitudeUpdated", "longitudeUpdated");

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<String> entity = new HttpEntity<>(cityJSON, headers);

        try {
            restTemplate.exchange(createUrl(put), HttpMethod.PUT, entity, String.class);
        } catch (HttpClientErrorException ex) {
            Assert.assertEquals(HttpStatus.CONFLICT, ex.getStatusCode());
        }
    }

    @Test
    public void t09ShouldNotUpdateCityNotFound() throws Exception {

        City city = new City(50L, "nameUpdated", "latitudeUpdated", "longitudeUpdated");

        String cityJSON = new ObjectMapper().writeValueAsString(city);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<String> entity = new HttpEntity<>(cityJSON, headers);

        try {
            restTemplate.exchange(createUrl(put), HttpMethod.PUT, entity, String.class);
        } catch (HttpClientErrorException ex) {
            Assert.assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }

    @Test
    public void t10ShouldDeleteCity() throws Exception {
        restTemplate.delete(createUrl(delete + 4L));

        try {
            restTemplate.getForEntity(createUrl(get + 4L), City.class);
        } catch (HttpClientErrorException ex) {
            Assert.assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }

    @Test
    public void t11ShouldGetAllCities() throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createUrl(getAll), HttpMethod.GET, entity, String.class);

        String result = response.getBody();

        Assert.assertTrue(result.contains(":1,"));
        Assert.assertTrue(result.contains(":2,"));
        Assert.assertTrue(result.contains(":3,"));
        Assert.assertFalse(result.contains(":4,"));
        Assert.assertTrue(result.contains(":5,"));
    }

    private String createUrl(String endpoint) {
        return "http://localhost:" + port + endpoint;
    }

}
