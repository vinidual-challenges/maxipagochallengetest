package br.com.maxipago.exception;

import br.com.maxipago.message.MessagesValidation;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CityConflictException {

    public static void createIdNotNull(){
        throw new ResponseStatusException(HttpStatus.CONFLICT, MessagesValidation.CITY_ID_MUST_BE_NULL);
    }

    public static void updateIdIsNull() {
        throw new ResponseStatusException(HttpStatus.CONFLICT, MessagesValidation.CITY_ID_MUST_NOT_BE_NULL);
    }
}
