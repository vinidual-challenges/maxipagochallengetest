package br.com.maxipago.service;

import br.com.maxipago.exception.CityConflictException;
import br.com.maxipago.message.MessagesStreams;
import br.com.maxipago.model.City;
import br.com.maxipago.repository.CityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;

@Service
public class CityService {

    private Logger logger = LoggerFactory.getLogger(CityService.class);

    @Autowired
    CityRepository cityRepository;

    public City create(City city){
        logger.info(MessagesStreams.SERVICE_POST_CITY.value);

        City created = cityRepository.create(city);

        if(Objects.isNull(created)) {
            CityConflictException.createIdNotNull();
        }

        return created;
    }

    public City get(Long id){
        logger.info(MessagesStreams.SERVICE_GET_CITY.value.concat(id.toString()));
        return cityRepository.get(id);
    }

    public Set<City> getAll(){
        logger.info(MessagesStreams.SERVICE_GET_ALL_CITIES.value);
        return cityRepository.getAll();
    }

    public City update(City city){
        logger.info(MessagesStreams.SERVICE_UPDATE_CITY.value + city.getId());

        if(Objects.isNull(city.getId())){
            CityConflictException.updateIdIsNull();
        }
        return cityRepository.update(city);
    }

    public void delete(Long id){
        logger.info(MessagesStreams.SERVICE_DELETE_CITY.value);
        cityRepository.delete(id);
    }

}
