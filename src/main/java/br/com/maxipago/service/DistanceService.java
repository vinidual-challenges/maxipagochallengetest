package br.com.maxipago.service;

import br.com.maxipago.controller.DistanceController;
import br.com.maxipago.message.MessagesStreams;
import br.com.maxipago.model.City;
import br.com.maxipago.model.Distance;
import br.com.maxipago.model.MetricUnit;
import br.com.maxipago.repository.CityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DistanceService {

    private Logger logger = LoggerFactory.getLogger(DistanceService.class);

    @Autowired
    CityRepository cityRepository;

    public Set<Distance> getAllDistances(MetricUnit metricUnit) {

        logger.info(MessagesStreams.SERVICE_GET_ALL_DISTANCES.value);

        Set<Distance> distances = new HashSet<>();

        List<City> cities = cityRepository.getAll().stream().collect(Collectors.toList());

        for (int i = 0; i < cities.size() - 1; i++) {

            for (int j = i + 1; j < cities.size(); j++) {

                logger.info(MessagesStreams.SERVICE_GET_ALL_DISTANCES_BETWEEN.value +
                        cities.get(i).getName() + " and " + cities.get(j).getName() );

                Distance distance = calculateHaversine(cities.get(i), cities.get(j), metricUnit);

                distances.add(distance);
            }

        }
        return distances;
    }

    private Distance calculateHaversine(City origenCity, City destinyCity, MetricUnit metricUnit) {

        Integer earthRadius = 6371;

        Double origenLatRad = Math.toRadians(Double.valueOf(origenCity.getLatitude()));
        Double origenLonRad = Math.toRadians(Double.valueOf(origenCity.getLongitude()));

        Double destinyLatRad = Math.toRadians(Double.valueOf(destinyCity.getLatitude()));
        Double destinyLonRad = Math.toRadians(Double.valueOf(destinyCity.getLongitude()));

        Double haversineRad = haversine(destinyLatRad - origenLatRad) +
                Math.cos(origenLatRad) * Math.cos(destinyLatRad) * haversine(origenLonRad - destinyLonRad);

        Double distance = 2 * earthRadius * Math.asin(Math.sqrt(haversineRad));

        if(MetricUnit.MI.equals(metricUnit)){
            distance *= 0.621371;
        }

        Distance distanceObject =
                new Distance(origenCity.getName(), destinyCity.getName(), distance, metricUnit.name());

        return distanceObject;
    }

    private Double haversine(Double theta){
        return ((1 - Math.cos(theta)) / 2);
    }

}
