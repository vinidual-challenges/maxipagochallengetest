package br.com.maxipago.message;

public class MessagesValidation {

    public static final String CITY_ID_MUST_BE_NULL = "City ID must be null when creating";
    public static final String CITY_ID_MUST_NOT_BE_NULL = "City ID cannot be null when updating";
    public final static String CITY_NAME_MUST_NOT_BE_NULL = "City NAME cannot not be null or empty";
    public final static String CITY_LATITUDE_MUST_NOT_BE_NULL = "City LATITUDE cannot not be null or empty";
    public final static String CITY_LONGITUDE_MUST_NOT_BE_NULL = "City LONGITUDE cannot be null or empty";

}
