package br.com.maxipago.message;

public class QueriesDatabase {

    public static final String CREATE_TABLE_CITY
            = "CREATE TABLE IF NOT EXISTS city " +
            "(id BIGINT  PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255) NOT NULL, " +
            "latitude VARCHAR(255) NOT NULL, longitude VARCHAR(255) NOT NULL)";

    public static final String DROP_TABLE_CITY = "DROP TABLE IF EXISTS city";
}
