package br.com.maxipago;

import br.com.maxipago.message.QueriesDatabase;
import br.com.maxipago.model.City;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    DataSource dataSource;

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public Map<String, Object> createCityParameters(City city){
        Map<String, Object> parameters = new HashMap<>();

        parameters.put("ID", city.getId());
        parameters.put("NAME", city.getName());
        parameters.put("LATITUDE", city.getLatitude());
        parameters.put("LONGITUDE", city.getLongitude());

        return parameters;
    }

    @Override
    public void run(String... args) throws Exception {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("city");

        log.info(QueriesDatabase.DROP_TABLE_CITY);
        jdbcTemplate.execute(QueriesDatabase.DROP_TABLE_CITY);

        log.info(QueriesDatabase.CREATE_TABLE_CITY);
        jdbcTemplate.execute(QueriesDatabase.CREATE_TABLE_CITY);

        City saoPaulo = new City(null, "Sao Paulo", "-23.5505", "-46.6333");
        City rioDeJaneiro = new City(null, "Rio de Janeiro", "-22.9035", "-43.2096");
        City nagasaki = new City(null, "Nagasaki", "32.764233", "129.872696");

        simpleJdbcInsert.execute(createCityParameters(saoPaulo));
        simpleJdbcInsert.execute(createCityParameters(rioDeJaneiro));
        simpleJdbcInsert.execute(createCityParameters(nagasaki));
    }

}