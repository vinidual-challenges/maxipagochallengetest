package br.com.maxipago.model;

public class Distance {

    String cityOrigin;

    String cityDestiny;

    Double distance;

    String metricUnit;

    public Distance(String cityOrigin, String cityDestiny, Double distance, String metricUnit){
        this.cityOrigin = cityOrigin;
        this.cityDestiny = cityDestiny;
        this.distance = distance;
        this.metricUnit = metricUnit;
    }

    public String getCityOrigin() {
        return cityOrigin;
    }

    public void setCityOrigin(String cityOrigin) {
        this.cityOrigin = cityOrigin;
    }

    public String getCityDestiny() {
        return cityDestiny;
    }

    public void setCityDestiny(String cityDestiny) {
        this.cityDestiny = cityDestiny;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getMetricUnit() {
        return metricUnit;
    }

    public void setMetricUnit(String metricUnit) {
        this.metricUnit = metricUnit;
    }

}
