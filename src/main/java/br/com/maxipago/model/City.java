package br.com.maxipago.model;

import br.com.maxipago.message.MessagesValidation;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotEmpty;

public class City {

    @Nullable
    private Long id;

    @NotEmpty(message = MessagesValidation.CITY_NAME_MUST_NOT_BE_NULL)
    private String name;

    @NotEmpty(message = MessagesValidation.CITY_LATITUDE_MUST_NOT_BE_NULL)
    private String latitude;

    @NotEmpty(message = MessagesValidation.CITY_LONGITUDE_MUST_NOT_BE_NULL)
    private String longitude;

    public City(){

    }

    public City(Long id, String name, String latitude, String longitude){
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
