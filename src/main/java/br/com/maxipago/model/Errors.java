package br.com.maxipago.model;

import java.util.HashMap;
import java.util.Map;

public class Errors {

    private Map<String, String> errors = new HashMap<>();

    public void addMapError(Map<String, String> map){
        this.errors.putAll(map);
    }

    public Map<String, String> getErrors(){
        return errors;
    }

}
