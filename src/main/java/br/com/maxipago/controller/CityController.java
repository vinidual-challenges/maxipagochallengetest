package br.com.maxipago.controller;

import br.com.maxipago.message.MessagesStreams;
import br.com.maxipago.model.City;
import br.com.maxipago.service.CityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;
import java.util.Set;

@RestController
public class CityController {

    private Logger logger = LoggerFactory.getLogger(CityController.class);

    @Autowired
    CityService cityService;

    @PostMapping("/addNewCity")
    public ResponseEntity<City> create(@Valid @RequestBody City city) {
        logger.info(MessagesStreams.CONTROLLER_POST_CITY.value);
        return new ResponseEntity<>(cityService.create(city), HttpStatus.CREATED);
    }

    @GetMapping("/getCity/{id}")
    public ResponseEntity<City> get(@PathVariable Long id) {
        logger.info(MessagesStreams.CONTROLLER_GET_CITY.value.concat(id.toString()));

        City found = cityService.get(id);
        if(Objects.isNull(found)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cityService.get(id), HttpStatus.OK);
    }

    @GetMapping("/getAllCities")
    public ResponseEntity<Set<City>> getAll() {
        logger.info(MessagesStreams.CONTROLLER_GET_ALL_CITIES.value);
        return new ResponseEntity<>(cityService.getAll(), HttpStatus.OK);
    }

    @PutMapping("/updateCity")
    public ResponseEntity<City> update(@Valid @RequestBody City city) {
        logger.info(MessagesStreams.CONTROLLER_UPDATE_CITY.value + city.getId());

        City updated = cityService.update(city);
        if(Objects.isNull(updated)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cityService.update(city), HttpStatus.OK);
    }

    @DeleteMapping("/deleteCity/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        logger.info(MessagesStreams.CONTROLLER_DELETE_CITY.value + id);
        cityService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}