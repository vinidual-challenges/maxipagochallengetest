package br.com.maxipago.controller;

import br.com.maxipago.message.MessagesStreams;
import br.com.maxipago.model.Distance;
import br.com.maxipago.model.MetricUnit;
import br.com.maxipago.service.DistanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.Set;

@RestController
public class DistanceController {

    @Autowired
    DistanceService distanceService;

    private Logger logger = LoggerFactory.getLogger(DistanceController.class);

    @GetMapping("/getAllDistances")
    public ResponseEntity<Set<Distance>> getAll(@RequestParam MetricUnit metricUnit) {
        logger.info(MessagesStreams.CONTROLLER_GET_ALL_DISTANCES.value);
        return new ResponseEntity<>(distanceService.getAllDistances(metricUnit), HttpStatus.OK);
    }

}
