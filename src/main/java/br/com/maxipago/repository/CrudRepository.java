package br.com.maxipago.repository;

import java.util.Set;

public interface CrudRepository<T> {

    T create(T city);

    T get(Long id);

    Set<T> getAll();

    T update(T city);

    void delete(Long id);

}
