package br.com.maxipago.repository;

import br.com.maxipago.message.MessagesStreams;
import br.com.maxipago.model.City;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class CityRepository implements CrudRepository<City> {

    private Logger logger = LoggerFactory.getLogger(CityRepository.class);

    @Autowired
    DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert jdbcInsert;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("city")
            .usingGeneratedKeyColumns("ID");
    }

    @Override
    public City create(City city){
        logger.info(MessagesStreams.REPOSITORY_POST_CITY.value);

        if(!Objects.isNull(city.getId())){
            return null;
        }

        SqlParameterSource parameters = new BeanPropertySqlParameterSource(city);
        Long id = jdbcInsert.executeAndReturnKey(parameters).longValue();
        city.setId(id);

        return !Objects.isNull(id) ? city : null;
    }

    @Override
    public City get(Long id){
        logger.info(MessagesStreams.REPOSITORY_GET_CITY.value.concat(id.toString()));

        String sql = "SELECT * FROM city WHERE id = ?";

        try {
            return jdbcTemplate.queryForObject(sql, new Object[] { id }, new CityRowMapper());
        }
        catch (EmptyResultDataAccessException ex){
            return null;
        }
    }

    @Override
    public Set<City> getAll(){
        logger.info(MessagesStreams.REPOSITORY_GET_ALL_CITIES.value);

        String sql = "SELECT * FROM city";

        List<City> cities = jdbcTemplate.query(sql, new CityRowMapper());

        return cities.stream().collect(Collectors.toSet());
    }

    @Override
    public City update(City city){
        logger.info(MessagesStreams.REPOSITORY_UPDATE_CITY.value.concat(city.getId().toString()));

        String sql = "UPDATE city SET name = ?, latitude = ?, longitude = ? WHERE id = ?";
        Integer id = jdbcTemplate.update(sql, city.getName(), city.getLatitude(), city.getLongitude(), city.getId());

        return id.equals(1) ? city : null;
    }

    @Override
    public void delete(Long id){
        logger.info(MessagesStreams.REPOSITORY_DELETE_CITY.value);

        String sql = "DELETE FROM city WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

}
