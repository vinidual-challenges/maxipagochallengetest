## Introduction

This was a test conducted as a first evaluation phase for a Java developer job, without specifying the job level.

## Goal

Provide webservice that returns list with combination of city pairs and distance between them.

## Requirements

- RESTful Webservice
- Java 8 or higher
- Tomcat 8 or higher
- Required parameter to set the unit of measurement (km / mi)
- Required parameter to set response format (xml / json)
- MySQL database with only one table (CITY [id, name, latitude, longitude]) and with a minimum of 3 records
- Use only JDBC access - Do not use persistence framework, connection pool or the like.
- You can search the web by distance calculation method, but you need to justify your choice.
- Justify the combination method implemented with a focus on performance and simplicity.
- Deliver two text files with the project README.TXT and LEIAME.TXT containing the justifications, scripts and observations in English and Portuguese, respectively.
- The project must be delivered with the source code in the war file.

## Solution

The solution was built using Java 11 with embedded tomcat and spring boot.

A restfull api for a CRUD for cities following good practices of maturity levels was built through the following article:

- https://blog.restcase.com/4-maturity-levels-of-rest-api-design/

For the calculation of the distance between pairs of cities the Haversine formula was used through the article:

- https://en.wikipedia.org/wiki/Haversine_formula

The method presents interesting results with good approximations for floating points, and is significantly simple to implement in any programming language.

To assist in testing the api you can consult the requests that the application uses in the following article:

- https://documenter.getpostman.com/view/1454530/S1LvVTxA#af71304a-d93a-4ff2-bcf8-97ee015efb6b

The tests were performed in a local environment.

Note that the parameters for requesting the response in JSON or XML format is defined by the ACCEPT header that is defined by the following values:

- application/json
- application/xml

## Setup

You need to set up a local mysql database to run the integration tests and upload the application:

- spring.datasource.driver = com.mysql.cj.jdbc.Driver
- spring.datasource.url = jdbc: mysql: // localhost: 3306 / maxipago
- spring.datasource.username = maxipago
- spring.datasource.password = maxipago